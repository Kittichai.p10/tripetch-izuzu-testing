import './App.css';
import TripetchIzuzuTest from './components/TripetchIzuzuTest';

function App() {
  return (
    <div className="App">
      <TripetchIzuzuTest />
    </div>
  );
}

export default App;
