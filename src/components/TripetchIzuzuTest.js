import React from "react";
import { useDeviceInfo } from '../utils/useDeviceInfo';
import Carousel from 'react-material-ui-carousel';

const TripetchIzuzuTest = () => {
  const { isMobile, isTablet } = useDeviceInfo();

  return (
    <div className="tripetch-izuzu">
      {!isMobile ? (
        <>
          <div className="athlets-container">
            <div className="athlets-box1">
              <div style={{ width: isTablet ? '40%' : '50%'  }} />
              <div 
                style={{ 
                  width: isTablet ? '60%' : '50%',
                  zIndex: 10, 
                  position: "relative"
                }}
              >
                <div className="title">
                  ATHLETS
                </div>
                <div className="box-athlets-text1">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">01</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    CONNECTION
                  </div>
                </div>
                <div className="text1">
                  Connect with coaches directly, you can ping coaches to view profile.
                </div>
              </div>
            </div>
            <div className="athlets-box2">
              <div style={{ width: isTablet ? '40%' : '50%' }} />
              <div 
                style={{ width: isTablet ? '60%' : '50%',
                zIndex: 10, 
                position: "relative"
              }}
             >
                <div className="box-text2">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">02</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    COLLABORATION
                  </div>
                </div>
                <div className="text1">
                  Work with other student athletes to increase visability. When you share and like other players videos it will increase your visability as a player. This is the team work aspect to Surface 1.
                </div>
              </div>
            </div>
            <div className="athlets-box3">
              <div style={{ width: isTablet ? '40%' : '50%' }} />
              <div 
                style={{ 
                  width: isTablet ? '60%' : '50%',
                  zIndex: 10, 
                  position: "relative"
                }}
              >
                <div className="box-text2">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">03</div>
                    <div className="line2" />
                  </div>
                  <div className="label">
                    GROWTH
                  </div>
                </div>
                <div className="text2">
                  Resources and tools for you to get better as a student Athelte. Access to training classes, tutor sessions, etc             
                </div>
              </div>
            </div>
            <div className="image-athlets">
              {isTablet ? (
                <img
                  src="/assets/images/img_Group4.svg"
                  alt="Group4"
                />
              ) : (
                <img
                  src="/assets/images/img_footballer1.svg"
                  alt="footballer"
                />
              )}
            </div>
          </div>
          <div className="players-container">
            <div className="players-box1">
              <div style={{ width: '50%', zIndex: 10, position: "relative" }}>
                <div className="title">
                  PlAYERS 
                </div>
                <div className="box-players-text1">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">01</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    CONNECTION
                  </div>
                </div>
                <div className="text1">
                  Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.
                </div>
              </div>
            </div>

            <div className="players-box2">
              <div style={{ width: '50%', zIndex: 10, position: "relative" }}>
                <div className="box-text2">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">02</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    COLLABORATION
                  </div>
                </div>
                <div className="text1">
                  Work with recruiter to increase your chances of finding talented athlete.
                </div>
              </div>
            </div>

            <div className="players-box3">
              <div style={{ width: '50%', zIndex: 10, position: "relative" }}>
                <div className="box-text2">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number-players-growth">03</div>
                    <div className="line2" />
                  </div>
                  <div className="label">
                    GROWTH
                  </div>
                </div>
                <div className="text2">
                  Save your time, recruit proper athlets for your team.
                </div>
              </div>
            </div>

            <div className="image-players">
              {isTablet ? (
                <img
                  src="/assets/images/img_Group5.svg"
                  alt="Group5"
                />
              ) : (
                <img
                  src="/assets/images/img_Group3.svg"
                  alt="Group3"
                />
              )}

            </div>
          </div>
        </>
      ) : (
        <div className="mobile">
          <div className="mobile-athlets">
            <div className="title">
              ATHLETS
            </div>
            <div className="image-mobile">
              <img
                src="/assets/images/img_Group6.svg"
                alt="Group6"
              />
            </div>
          </div>

          <div className="mobile-carousel">
            <Carousel 
              className="carousel"
              indicators={true} 
              swipe={true} 
              autoPlay={false}
              animation="slide"
            >
              <div className="mobile-carousel-box">
                <div className="mobile-box-text">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">01</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    CONNECTION
                  </div>
                </div>
                <div className="text1">
                  Connect with coaches directly, you can ping coaches to view profile.
                </div>
              </div>
              <div className="mobile-carousel-box">
                <div className="mobile-box-text">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">02</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    COLLABORATION
                  </div>
                </div>
                <div className="text1">
                  Work with other student athletes to  increase visability. When you share and like other players videos it will increase your visability as a player. This is the team work aspect to Surface 1.
                </div>
              </div>
              <div className="mobile-carousel-box">
                <div className="mobile-box-text">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">03</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    GROWTH
                  </div>
                </div>
                <div className="text1">
                  Resources and tools for you to get better as a student Athelte. Access to training classes, tutor sessions, etc                 </div>
              </div>
            </Carousel>
          </div>

          <div className="mobile-players">
            <div className="title-players">
              PLAYERS
            </div>
            <div className="image-mobile">
              <img
                src="/assets/images/img_Group7.svg"
                alt="Group7"
              />
            </div>
          </div>

          <div className="mobile-carousel">
            <Carousel 
              className="carousel"
              indicators={true} 
              swipe={true} 
              autoPlay={false}
              animation="slide"
            >
              <div className="mobile-carousel-box">
                <div className="mobile-box-text">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">01</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    CONNECTION
                  </div>
                </div>
                <div className="text1">
                  Connect with talented athlete directly, you can watch their skills through video showreels directly from Surface 1.                </div>
              </div>
              <div className="mobile-carousel-box">
                <div className="mobile-box-text">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">02</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    COLLABORATION
                  </div>
                </div>
                <div className="text1">
                  Work with recruiter to increase your chances of findingtalented athlete.                </div>
              </div>
              <div className="mobile-carousel-box">
                <div className="mobile-box-text">
                  <div style={{ display: 'grid', marginRight: '10px' }}>
                    <div className="text-number">03</div>
                    <div className="line1" />
                  </div>
                  <div className="label">
                    GROWTH
                  </div>
                </div>
                <div className="text1">
                  Save your time, recruit proper athlets for your team.
                </div>
              </div>
            </Carousel>
          </div>
        </div>
      )}
    </div>
  );
};

export default TripetchIzuzuTest;
